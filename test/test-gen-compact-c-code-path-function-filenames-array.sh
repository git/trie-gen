#!/bin/bash

# Copyright (C) 2016, 2018, 2021  Stefan Vargyas
# 
# This file is part of Trie-Gen.
# 
# Trie-Gen is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Trie-Gen is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Trie-Gen.  If not, see <http://www.gnu.org/licenses/>.

#
# File generated by a command like:
# $ gen-test -T gen-compact-c-code-path-function-filenames-array
#

[[ "$1" =~ ^-u[0-9]+$ ]] &&
u="${1:2}" ||
u=""

diff -u$u -L gen-compact-c-code-path-function-filenames-array.old <(echo \
'$ print() { printf '\''%s\n'\'' "$@"; }
$ ../src/trie --attrs --dots --gen=compact --output=c-code --path=function --trie=array < filenames.txt
.   // [1,'\''c'\'']
.   // [0,"c++py-tree-demo",'\''/'\'',[3,'\''c'\'']]
.   if (prefix("c++py-tree-demo/", p)) {
.   .   p += 16;
.   .   switch (*p ++) {
.   .   // [15,"c++py-tree-demo.src",'\''-'\'',[2,'\''f'\'']]
.   .   case '\''.'\'':
.   .   .   if (prefix("src-", p)) {
.   .   .   .   p += 4;
.   .   .   .   switch (*p ++) {
.   .   .   .   // [19,"c++py-tree-demo.srcdir",'\''s'\'',[0]]
.   .   .   .   case '\''d'\'':
.   .   .   .   .   if (equal(p, "irs"))
.   .   .   .   .   .   return "c++py-tree-demo/.src-dirs";
.   .   .   .   .   return error;
.   .   .   .   // [19,"c++py-tree-demo.srcfile",'\''s'\'',[0]]
.   .   .   .   case '\''f'\'':
.   .   .   .   .   if (equal(p, "iles"))
.   .   .   .   .   .   return "c++py-tree-demo/.src-files";
.   .   .   .   }
.   .   .   }
.   .   .   return error;
.   .   // [15,"c++py-tree-demobin.patc",'\''h'\'',[0]]
.   .   case '\''b'\'':
.   .   .   if (equal(p, "in.patch"))
.   .   .   .   return "c++py-tree-demo/bin.patch";
.   .   .   return error;
.   .   // [15,"c++py-tree-democ++p",'\''y'\'',[2,'\''/'\'']]
.   .   case '\''c'\'':
.   .   .   if (prefix("++py", p)) {
.   .   .   .   p += 4;
.   .   .   .   switch (*p ++) {
.   .   .   .   // [19,"c++py-tree-democ++p-tre",'\''e'\'',[2,'\''.'\'']]
.   .   .   .   case '\''-'\'':
.   .   .   .   .   if (prefix("tree", p)) {
.   .   .   .   .   .   p += 4;
.   .   .   .   .   .   switch (*p ++) {
.   .   .   .   .   .   // [23,"c++py-tree-democ++p-tre",'\''-'\'',[3,'\''r'\'']]
.   .   .   .   .   .   case '\''-'\'':
.   .   .   .   .   .   .   switch (*p ++) {
.   .   .   .   .   .   .   // [23,"c++py-tree-democ++p-treauthor.tx",'\''t'\'',[0]]
.   .   .   .   .   .   .   case '\''a'\'':
.   .   .   .   .   .   .   .   if (equal(p, "uthor.txt"))
.   .   .   .   .   .   .   .   .   return "c++py-tree-demo/c++py-tree-author.txt";
.   .   .   .   .   .   .   .   return error;
.   .   .   .   .   .   .   // [23,"c++py-tree-democ++p-tredemo.tx",'\''t'\'',[0]]
.   .   .   .   .   .   .   case '\''d'\'':
.   .   .   .   .   .   .   .   if (equal(p, "emo.txt"))
.   .   .   .   .   .   .   .   .   return "c++py-tree-demo/c++py-tree-demo.txt";
.   .   .   .   .   .   .   .   return error;
.   .   .   .   .   .   .   // [23,"c++py-tree-democ++p-trereadme",'\''.'\'',[2,'\''t'\'']]
.   .   .   .   .   .   .   case '\''r'\'':
.   .   .   .   .   .   .   .   if (prefix("eadme.", p)) {
.   .   .   .   .   .   .   .   .   p += 6;
.   .   .   .   .   .   .   .   .   switch (*p ++) {
.   .   .   .   .   .   .   .   .   // [29,"c++py-tree-democ++p-trereadmepd",'\''f'\'',[0]]
.   .   .   .   .   .   .   .   .   case '\''p'\'':
.   .   .   .   .   .   .   .   .   .   if (equal(p, "df"))
.   .   .   .   .   .   .   .   .   .   .   return "c++py-tree-demo/c++py-tree-readme.pdf";
.   .   .   .   .   .   .   .   .   .   return error;
.   .   .   .   .   .   .   .   .   // [29,"c++py-tree-democ++p-trereadme",'\''t'\'',[2,'\''x'\'']]
.   .   .   .   .   .   .   .   .   case '\''t'\'':
.   .   .   .   .   .   .   .   .   .   switch (*p ++) {
.   .   .   .   .   .   .   .   .   .   // [29,"c++py-tree-democ++p-trereadmee",'\''x'\'',[0]]
.   .   .   .   .   .   .   .   .   .   case '\''e'\'':
.   .   .   .   .   .   .   .   .   .   .   if (*p ++ == '\''x'\'' &&
.   .   .   .   .   .   .   .   .   .   .   .   *p == 0)
.   .   .   .   .   .   .   .   .   .   .   .   return "c++py-tree-demo/c++py-tree-readme.tex";
.   .   .   .   .   .   .   .   .   .   .   return error;
.   .   .   .   .   .   .   .   .   .   // [29,"c++py-tree-democ++p-trereadmex",'\''t'\'',[0]]
.   .   .   .   .   .   .   .   .   .   case '\''x'\'':
.   .   .   .   .   .   .   .   .   .   .   if (*p ++ == '\''t'\'' &&
.   .   .   .   .   .   .   .   .   .   .   .   *p == 0)
.   .   .   .   .   .   .   .   .   .   .   .   return "c++py-tree-demo/c++py-tree-readme.txt";
.   .   .   .   .   .   .   .   .   .   }
.   .   .   .   .   .   .   .   .   }
.   .   .   .   .   .   .   .   }
.   .   .   .   .   .   .   }
.   .   .   .   .   .   .   return error;
.   .   .   .   .   .   // [23,"c++py-tree-democ++p-tre",'\''.'\'',[2,'\''t'\'']]
.   .   .   .   .   .   case '\''.'\'':
.   .   .   .   .   .   .   switch (*p ++) {
.   .   .   .   .   .   .   // [23,"c++py-tree-democ++p-trepd",'\''f'\'',[0]]
.   .   .   .   .   .   .   case '\''p'\'':
.   .   .   .   .   .   .   .   if (equal(p, "df"))
.   .   .   .   .   .   .   .   .   return "c++py-tree-demo/c++py-tree.pdf";
.   .   .   .   .   .   .   .   return error;
.   .   .   .   .   .   .   // [23,"c++py-tree-democ++p-tre",'\''t'\'',[2,'\''x'\'']]
.   .   .   .   .   .   .   case '\''t'\'':
.   .   .   .   .   .   .   .   switch (*p ++) {
.   .   .   .   .   .   .   .   // [23,"c++py-tree-democ++p-tree",'\''x'\'',[0]]
.   .   .   .   .   .   .   .   case '\''e'\'':
.   .   .   .   .   .   .   .   .   if (*p ++ == '\''x'\'' &&
.   .   .   .   .   .   .   .   .   .   *p == 0)
.   .   .   .   .   .   .   .   .   .   return "c++py-tree-demo/c++py-tree.tex";
.   .   .   .   .   .   .   .   .   return error;
.   .   .   .   .   .   .   .   // [23,"c++py-tree-democ++p-trex",'\''t'\'',[0]]
.   .   .   .   .   .   .   .   case '\''x'\'':
.   .   .   .   .   .   .   .   .   if (*p ++ == '\''t'\'' &&
.   .   .   .   .   .   .   .   .   .   *p == 0)
.   .   .   .   .   .   .   .   .   .   return "c++py-tree-demo/c++py-tree.txt";
.   .   .   .   .   .   .   .   }
.   .   .   .   .   .   .   }
.   .   .   .   .   .   }
.   .   .   .   .   }
.   .   .   .   .   return error;
.   .   .   .   // [19,"c++py-tree-democ++p",'\''/'\'',[4,'\''R'\'']]
.   .   .   .   case '\''/'\'':
.   .   .   .   .   switch (*p ++) {
.   .   .   .   .   // [19,"c++py-tree-democ++p.src",'\''-'\'',[2,'\''f'\'']]
.   .   .   .   .   case '\''.'\'':
.   .   .   .   .   .   if (prefix("src-", p)) {
.   .   .   .   .   .   .   p += 4;
.   .   .   .   .   .   .   switch (*p ++) {
.   .   .   .   .   .   .   // [23,"c++py-tree-democ++p.srcdir",'\''s'\'',[0]]
.   .   .   .   .   .   .   case '\''d'\'':
.   .   .   .   .   .   .   .   if (equal(p, "irs"))
.   .   .   .   .   .   .   .   .   return "c++py-tree-demo/c++py/.src-dirs";
.   .   .   .   .   .   .   .   return error;
.   .   .   .   .   .   .   // [23,"c++py-tree-democ++p.srcfile",'\''s'\'',[0]]
.   .   .   .   .   .   .   case '\''f'\'':
.   .   .   .   .   .   .   .   if (equal(p, "iles"))
.   .   .   .   .   .   .   .   .   return "c++py-tree-demo/c++py/.src-files";
.   .   .   .   .   .   .   }
.   .   .   .   .   .   }
.   .   .   .   .   .   return error;
.   .   .   .   .   // [19,"c++py-tree-democ++pAUTHO",'\''R'\'',[0]]
.   .   .   .   .   case '\''A'\'':
.   .   .   .   .   .   if (equal(p, "UTHOR"))
.   .   .   .   .   .   .   return "c++py-tree-demo/c++py/AUTHOR";
.   .   .   .   .   .   return error;
.   .   .   .   .   // [19,"c++py-tree-democ++pMakefil",'\''e'\'',[0]]
.   .   .   .   .   case '\''M'\'':
.   .   .   .   .   .   if (equal(p, "akefile"))
.   .   .   .   .   .   .   return "c++py-tree-demo/c++py/Makefile";
.   .   .   .   .   .   return error;
.   .   .   .   .   // [19,"c++py-tree-democ++pREADM",'\''E'\'',[0]]
.   .   .   .   .   case '\''R'\'':
.   .   .   .   .   .   if (equal(p, "EADME"))
.   .   .   .   .   .   .   return "c++py-tree-demo/c++py/README";
.   .   .   .   .   }
.   .   .   .   }
.   .   .   }
.   .   }
.   }
.   return error;'
) -L gen-compact-c-code-path-function-filenames-array.new <(
echo '$ print() { printf '\''%s\n'\'' "$@"; }'
print() { printf '%s\n' "$@"; } 2>&1 ||
echo 'command failed: print() { printf '\''%s\n'\'' "$@"; }'

echo '$ ../src/trie --attrs --dots --gen=compact --output=c-code --path=function --trie=array < filenames.txt'
../src/trie --attrs --dots --gen=compact --output=c-code --path=function --trie=array < filenames.txt 2>&1 ||
echo 'command failed: ../src/trie --attrs --dots --gen=compact --output=c-code --path=function --trie=array < filenames.txt'
)

