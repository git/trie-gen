#!/bin/bash

# Copyright (C) 2016, 2018, 2021  Stefan Vargyas
# 
# This file is part of Trie-Gen.
# 
# Trie-Gen is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Trie-Gen is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Trie-Gen.  If not, see <http://www.gnu.org/licenses/>.

#
# File generated by a command like:
# $ gen-test -T gen-func2
#

[[ "$1" =~ ^-u[0-9]+$ ]] &&
u="${1:2}" ||
u=""

diff -u$u -L gen-func2.old <(echo \
'$ print() { printf '\''%s\n'\'' "$@"; }
$ . ../commands.sh
$ print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pe
int lookup(const char* p)
{
    switch (*p ++) {
    case '\''p'\'':
        if (*p ++ == '\''o'\'' &&
            *p ++ == '\''t'\'') {
            if (*p == 0)
                return 1;
            switch (*p ++) {
            case '\''a'\'':
                if (*p ++ == '\''t'\'' &&
                    *p ++ == '\''o'\'' &&
                    *p == 0)
                    return 2;
                return error;
            case '\''t'\'':
                if (*p ++ == '\''e'\'' &&
                    *p ++ == '\''r'\'' &&
                    *p ++ == '\''y'\'' &&
                    *p == 0)
                    return 3;
            }
        }
        return error;
    case '\''t'\'':
        switch (*p ++) {
        case '\''a'\'':
            if (*p ++ == '\''t'\'' &&
                *p ++ == '\''t'\'' &&
                *p ++ == '\''o'\'' &&
                *p ++ == '\''o'\'' &&
                *p == 0)
                return 4;
            return error;
        case '\''e'\'':
            if (*p ++ == '\''m'\'' &&
                *p ++ == '\''p'\'' &&
                *p ++ == '\''o'\'' &&
                *p == 0)
                return 5;
        }
    }
    return error;
}
$ print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pf
int lookup(const char* p)
{
    switch (*p ++) {
    case '\''p'\'':
        if (prefix("ot", p)) {
            p += 2;
            if (*p == 0)
                return 1;
            switch (*p ++) {
            case '\''a'\'':
                if (equal(p, "to"))
                    return 2;
                return error;
            case '\''t'\'':
                if (equal(p, "ery"))
                    return 3;
            }
        }
        return error;
    case '\''t'\'':
        switch (*p ++) {
        case '\''a'\'':
            if (equal(p, "ttoo"))
                return 4;
            return error;
        case '\''e'\'':
            if (equal(p, "mpo"))
                return 5;
        }
    }
    return error;
}
$ print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pe -z
int lookup(const char* p)
{
    switch (*p ++) {
    case '\''p'\'':
        if (*p ++ == '\''o'\'' &&
            *p ++ == '\''t'\'') {
            if (*p == 0)
                return 0;
            switch (*p ++) {
            case '\''a'\'':
                if (*p ++ == '\''t'\'' &&
                    *p ++ == '\''o'\'' &&
                    *p == 0)
                    return 1;
                return error;
            case '\''t'\'':
                if (*p ++ == '\''e'\'' &&
                    *p ++ == '\''r'\'' &&
                    *p ++ == '\''y'\'' &&
                    *p == 0)
                    return 2;
            }
        }
        return error;
    case '\''t'\'':
        switch (*p ++) {
        case '\''a'\'':
            if (*p ++ == '\''t'\'' &&
                *p ++ == '\''t'\'' &&
                *p ++ == '\''o'\'' &&
                *p ++ == '\''o'\'' &&
                *p == 0)
                return 3;
            return error;
        case '\''e'\'':
            if (*p ++ == '\''m'\'' &&
                *p ++ == '\''p'\'' &&
                *p ++ == '\''o'\'' &&
                *p == 0)
                return 4;
        }
    }
    return error;
}
$ print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pf -z
int lookup(const char* p)
{
    switch (*p ++) {
    case '\''p'\'':
        if (prefix("ot", p)) {
            p += 2;
            if (*p == 0)
                return 0;
            switch (*p ++) {
            case '\''a'\'':
                if (equal(p, "to"))
                    return 1;
                return error;
            case '\''t'\'':
                if (equal(p, "ery"))
                    return 2;
            }
        }
        return error;
    case '\''t'\'':
        switch (*p ++) {
        case '\''a'\'':
            if (equal(p, "ttoo"))
                return 3;
            return error;
        case '\''e'\'':
            if (equal(p, "mpo"))
                return 4;
        }
    }
    return error;
}
$ print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pe -u
int lookup(const char* p)
{
    switch (*p ++) {
    case '\''p'\'':
        if (*p ++ == '\''o'\'' &&
            *p ++ == '\''t'\'') {
            if (*p == 0)
                return 1;
            switch (*p ++) {
            case '\''a'\'':
                if ((*p == 0 || (*p ++ == '\''t'\'' &&
                    (*p == 0 || (*p ++ == '\''o'\'' &&
                    (*p == 0))))))
                    return 2;
                return error;
            case '\''t'\'':
                if ((*p == 0 || (*p ++ == '\''e'\'' &&
                    (*p == 0 || (*p ++ == '\''r'\'' &&
                    (*p == 0 || (*p ++ == '\''y'\'' &&
                    (*p == 0))))))))
                    return 3;
            }
        }
        return error;
    case '\''t'\'':
        switch (*p ++) {
        case '\''a'\'':
            if ((*p == 0 || (*p ++ == '\''t'\'' &&
                (*p == 0 || (*p ++ == '\''t'\'' &&
                (*p == 0 || (*p ++ == '\''o'\'' &&
                (*p == 0 || (*p ++ == '\''o'\'' &&
                (*p == 0))))))))))
                return 4;
            return error;
        case '\''e'\'':
            if ((*p == 0 || (*p ++ == '\''m'\'' &&
                (*p == 0 || (*p ++ == '\''p'\'' &&
                (*p == 0 || (*p ++ == '\''o'\'' &&
                (*p == 0))))))))
                return 5;
        }
    }
    return error;
}
$ print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pf -u
int lookup(const char* p)
{
    switch (*p ++) {
    case '\''p'\'':
        if (prefix("ot", p)) {
            p += 2;
            if (*p == 0)
                return 1;
            switch (*p ++) {
            case '\''a'\'':
                if (prefix(p, "to"))
                    return 2;
                return error;
            case '\''t'\'':
                if (prefix(p, "ery"))
                    return 3;
            }
        }
        return error;
    case '\''t'\'':
        switch (*p ++) {
        case '\''a'\'':
            if (prefix(p, "ttoo"))
                return 4;
            return error;
        case '\''e'\'':
            if (prefix(p, "mpo"))
                return 5;
        }
    }
    return error;
}
$ print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pe -uz
int lookup(const char* p)
{
    switch (*p ++) {
    case '\''p'\'':
        if (*p ++ == '\''o'\'' &&
            *p ++ == '\''t'\'') {
            if (*p == 0)
                return 0;
            switch (*p ++) {
            case '\''a'\'':
                if ((*p == 0 || (*p ++ == '\''t'\'' &&
                    (*p == 0 || (*p ++ == '\''o'\'' &&
                    (*p == 0))))))
                    return 1;
                return error;
            case '\''t'\'':
                if ((*p == 0 || (*p ++ == '\''e'\'' &&
                    (*p == 0 || (*p ++ == '\''r'\'' &&
                    (*p == 0 || (*p ++ == '\''y'\'' &&
                    (*p == 0))))))))
                    return 2;
            }
        }
        return error;
    case '\''t'\'':
        switch (*p ++) {
        case '\''a'\'':
            if ((*p == 0 || (*p ++ == '\''t'\'' &&
                (*p == 0 || (*p ++ == '\''t'\'' &&
                (*p == 0 || (*p ++ == '\''o'\'' &&
                (*p == 0 || (*p ++ == '\''o'\'' &&
                (*p == 0))))))))))
                return 3;
            return error;
        case '\''e'\'':
            if ((*p == 0 || (*p ++ == '\''m'\'' &&
                (*p == 0 || (*p ++ == '\''p'\'' &&
                (*p == 0 || (*p ++ == '\''o'\'' &&
                (*p == 0))))))))
                return 4;
        }
    }
    return error;
}
$ print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pf -uz
int lookup(const char* p)
{
    switch (*p ++) {
    case '\''p'\'':
        if (prefix("ot", p)) {
            p += 2;
            if (*p == 0)
                return 0;
            switch (*p ++) {
            case '\''a'\'':
                if (prefix(p, "to"))
                    return 1;
                return error;
            case '\''t'\'':
                if (prefix(p, "ery"))
                    return 2;
            }
        }
        return error;
    case '\''t'\'':
        switch (*p ++) {
        case '\''a'\'':
            if (prefix(p, "ttoo"))
                return 3;
            return error;
        case '\''e'\'':
            if (prefix(p, "mpo"))
                return 4;
        }
    }
    return error;
}'
) -L gen-func2.new <(
echo '$ print() { printf '\''%s\n'\'' "$@"; }'
print() { printf '%s\n' "$@"; } 2>&1 ||
echo 'command failed: print() { printf '\''%s\n'\'' "$@"; }'

echo '$ . ../commands.sh'
. ../commands.sh 2>&1 ||
echo 'command failed: . ../commands.sh'

echo '$ print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pe'
print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pe 2>&1 ||
echo 'command failed: print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pe'

echo '$ print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pf'
print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pf 2>&1 ||
echo 'command failed: print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pf'

echo '$ print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pe -z'
print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pe -z 2>&1 ||
echo 'command failed: print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pe -z'

echo '$ print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pf -z'
print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pf -z 2>&1 ||
echo 'command failed: print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pf -z'

echo '$ print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pe -u'
print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pe -u 2>&1 ||
echo 'command failed: print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pe -u'

echo '$ print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pf -u'
print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pf -u 2>&1 ||
echo 'command failed: print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pf -u'

echo '$ print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pe -uz'
print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pe -uz 2>&1 ||
echo 'command failed: print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pe -uz'

echo '$ print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pf -uz'
print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pf -uz 2>&1 ||
echo 'command failed: print pot potato pottery tattoo tempo|gen-func -h .. -r- -Pf -uz'
)

