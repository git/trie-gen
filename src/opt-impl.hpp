// Copyright (C) 2016, 2018, 2021  Stefan Vargyas
// 
// This file is part of Trie-Gen.
// 
// Trie-Gen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Trie-Gen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Trie-Gen.  If not, see <http://www.gnu.org/licenses/>.

static inline bool prefix(
    const char* p, const char* q)
{
    while (*p && *p == *q)
         ++ p, ++ q;
    return *p == 0;
}

// # long-opts [-n]
// $ long-opts() { local nrex='(::)?[a-zA-Z_][a-zA-Z0-9_]*(::[a-zA-Z_][a-zA-Z0-9_]*)*'; ${GCC:-gcc} -DDEBUG -std=gnu++11 -DPROGRAM=trie -E opt.cpp -o -|ssed -nR '/\slongs\s*\[\]\s*=\s*\{\s*$/,/^\s+\}\s*;\s*$/{:0;/^\s*(\{\s*".*?"\s*,\s*\d+\s*,\s*(?:\&\s*)?'"$nrex"'\s*,\s*'"$nrex"'\s*\}\s*,)/!b;s//\1\n/;s/" "//;P;s/^.*?\n//;b0}'|if [ "$1" == '-n' ]; then ssed -nR 's/^.*?"(.*?)".*?$/\1/p'; else cat; fi; }

// $ gen-long-opts-func() { long-opts -n|gen-func -r- -f lookup_long_opt -e-1 -Pf -uz|ssed -R '1s/^/static /'; }

// $ long-opts|less
// $ long-opts -n|less
// $ gen-long-opts-func 

static int lookup_long_opt(const char* p)
{
    switch (*p ++) {
    case 'a':
        if (prefix("ttr", p)) {
            p += 3;
            switch (*p ++) {
            case 'i':
                if (prefix(p, "butes"))
                    return 17;
                return -1;
            case 's':
                if (*p == 0)
                    return 18;
            }
        }
        return -1;
    case 'd':
        switch (*p ++) {
        case 'e':
            switch (*p ++) {
            case 'b':
                if (prefix(p, "ug"))
                    return 32;
                return -1;
            case 'p':
                if (prefix(p, "th"))
                    return 15;
            }
            return -1;
        case 'o':
            if (prefix(p, "ts"))
                return 29;
            return -1;
        case 'u':
            if (prefix("mp-opt", p)) {
                p += 6;
                switch (*p ++) {
                case 'i':
                    if (prefix(p, "ons"))
                        return 34;
                    return -1;
                case 's':
                    if (*p == 0)
                        return 35;
                }
            }
        }
        return -1;
    case 'e':
        switch (*p ++) {
        case 'q':
            if (prefix(p, "ual-func"))
                return 5;
            return -1;
        case 's':
            if (prefix(p, "cape-chars"))
                return 11;
            return -1;
        case 'x':
            if (prefix("pr", p)) {
                p += 2;
                if (*p == 0)
                    return 12;
                if (*p ++ == '-') {
                    switch (*p ++) {
                    case 'd':
                        if (prefix(p, "epth"))
                            return 14;
                        return -1;
                    case 't':
                        if (prefix(p, "ype"))
                            return 13;
                    }
                }
            }
        }
        return -1;
    case 'g':
        if (prefix(p, "en-type"))
            return 3;
        return -1;
    case 'h':
        if (prefix(p, "elp"))
            return 39;
        return -1;
    case 'n':
        if (*p ++ == 'o') {
            switch (*p ++) {
            case '-':
                switch (*p ++) {
                case 'a':
                    if (prefix("ttr", p)) {
                        p += 3;
                        switch (*p ++) {
                        case 'i':
                            if (prefix(p, "butes"))
                                return 20;
                            return -1;
                        case 's':
                            if (*p == 0)
                                return 21;
                        }
                    }
                    return -1;
                case 'd':
                    switch (*p ++) {
                    case 'e':
                        if (prefix(p, "bug"))
                            return 33;
                        return -1;
                    case 'o':
                        if (prefix(p, "ts"))
                            return 31;
                    }
                    return -1;
                case 'p':
                    if (prefix("rint-", p)) {
                        p += 5;
                        switch (*p ++) {
                        case 'a':
                            if (prefix(p, "ttributes"))
                                return 19;
                            return -1;
                        case 'd':
                            if (prefix(p, "ots"))
                                return 30;
                            return -1;
                        case 's':
                            if (prefix(p, "tatistics"))
                                return 25;
                        }
                    }
                    return -1;
                case 's':
                    if (prefix("tat", p)) {
                        p += 3;
                        switch (*p ++) {
                        case 'i':
                            if (prefix(p, "stics"))
                                return 26;
                            return -1;
                        case 's':
                            if (*p == 0)
                                return 27;
                        }
                    }
                    return -1;
                case 'u':
                    if (prefix(p, "nique-prefix"))
                        return 8;
                    return -1;
                case 'v':
                    if (prefix(p, "erbose"))
                        return 37;
                    return -1;
                case 'z':
                    if (prefix(p, "ero-start"))
                        return 10;
                }
                return -1;
            case 'd':
                if (prefix(p, "e-type"))
                    return 0;
            }
        }
        return -1;
    case 'o':
        if (prefix(p, "utput-type"))
            return 2;
        return -1;
    case 'p':
        switch (*p ++) {
        case 'a':
            if (prefix(p, "th-type"))
                return 4;
            return -1;
        case 'r':
            switch (*p ++) {
            case 'e':
                if (prefix(p, "fix-func"))
                    return 6;
                return -1;
            case 'i':
                if (prefix("nt-", p)) {
                    p += 3;
                    switch (*p ++) {
                    case 'a':
                        if (prefix(p, "ttributes"))
                            return 16;
                        return -1;
                    case 'd':
                        if (prefix(p, "ots"))
                            return 28;
                        return -1;
                    case 's':
                        if (prefix(p, "tatistics"))
                            return 22;
                    }
                }
            }
        }
        return -1;
    case 's':
        if (prefix("tat", p)) {
            p += 3;
            switch (*p ++) {
            case 'i':
                if (prefix(p, "stics"))
                    return 23;
                return -1;
            case 's':
                if (*p == 0)
                    return 24;
            }
        }
        return -1;
    case 't':
        if (prefix(p, "rie-type"))
            return 1;
        return -1;
    case 'u':
        if (prefix(p, "nique-prefix"))
            return 7;
        return -1;
    case 'v':
        if (prefix("er", p)) {
            p += 2;
            switch (*p ++) {
            case 'b':
                if (prefix(p, "ose"))
                    return 36;
                return -1;
            case 's':
                if (prefix(p, "ion"))
                    return 38;
            }
        }
        return -1;
    case 'z':
        if (prefix(p, "ero-start"))
            return 9;
    }
    return -1;
}


