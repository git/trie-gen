// Copyright (C) 2016, 2018, 2021  Stefan Vargyas
// 
// This file is part of Trie-Gen.
// 
// Trie-Gen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Trie-Gen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Trie-Gen.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __CONFIG_H

#ifndef __GNUC__
#error we need a GCC compiler
#endif

#ifndef __GNUC_MINOR__
#error __GNUC_MINOR__ is not defined
#endif

#ifndef __GNUC_PATCHLEVEL__
#error __GNUC_PATCHLEVEL__ is not defined
#endif

#define GCC_VERSION \
    (__GNUC__ * 10000 + __GNUC_MINOR__ * 100 + __GNUC_PATCHLEVEL__)

//
// CONFIG_VA_END_NOOP
//

// stev: CONFIG_VA_END_NOOP establishes whether the compiler does
// not require that invocations of 'va_start' macro be matched by
// a corresponding 'va_end' call -- in other words, the parameter
// shall evaluate to 'true' iff 'va_end' is a no-operation macro
// (or function). Note that the C standard (C99 7.15.1, C11 7.16.1)
// says that each call to 'va_start' shall be paired by a call to
// 'va_end'.

// stev: the implementation of GCC v4.0 up to v7.3.0 boils down
// the 'va_end' macro to a built-in function '__builtin_va_end'.
// This function is indeed a trivial no-operation (look into the
// source code gcc/builtins.{def,c}). Other versions of GCC might
// well implement the same behaviour -- but, since this issue is
// a tricky one, CONFIG_VA_END_NOOP should only be set to 'true'
// following a positive assurance about the things implied.
#if GCC_VERSION < 40000 || GCC_VERSION > 80201 
#define CONFIG_VA_END_NOOP 0
#else
#define CONFIG_VA_END_NOOP 1
#endif

#endif /* __CONFIG_H */

